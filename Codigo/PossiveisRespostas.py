import asyncio
import random
import Google
def respostas(cliente_discord):
    @cliente_discord.event
    async def on_message(texto):
        if (texto.author.id == 'ID'): return
        if texto.content.startswith('Bot'):  # Se uma frase tiver Bot no inico
            frase = texto.content[3:].strip()  # Tira 'Bot' da frase
            frase = frase.lower()
            if frase.startswith('dado'):  # ATira dado de 1 a 6
                numr = random.randint(1, 6)
                await texto.channel.send(str(numr))
                return
            elif frase.endswith('?'):
                #Problems
                if frase.startswith('qual seu nome') or  frase.startswith('quem é você'):
                    resposta = cliente_discord.user.name
                    await texto.channel.send(resposta)
                elif frase.startswith('quem é seu pai'):
                    resposta = 'Hudson'
                    await texto.channel.send(resposta)
                elif frase.startswith('o que você acha da vithoria'):
                    resposta = '💁🏽 Uma Pessoa que jamais será derrotada💁‍♀️ '
                    await texto.channel.send(resposta)
                else:
                    resposta = random.choice(['Não respondo a isso', 'Sim', 'As vezes', 'Não', 'Claro', 'NUNCA!', 'Um dia talvez','A resposta está dentro de ti', 'Mais ou menos', 'Uma Bosta', 'Podia ser pior'])
                    await texto.channel.send(resposta)
                return
            elif frase.endswith('belford roxo'):
                resposta = 'Temos uma bifrost'
                await texto.channel.send(resposta)
            elif frase.endswith('luiz'):
                resposta = 'BRUM BRUM BRUM 🚗'
                await texto.channel.send(resposta)
            elif frase.endswith('yago'):
                resposta = 'Tiozão 👴🏾'
                await texto.channel.send(resposta)
            elif frase.startswith("bundão"):
                resposta = 'https://www.youtube.com/watch?v=w_ejwSACf_U'
                await texto.channel.send(resposta)
            elif frase.startswith('moeda'):
                resp = random.randint(1, 2)
                if resp == 1:
                    resposta = '👦🏽'
                    await texto.channel.send(resposta)
                else:
                    resposta ='🤴🏽'
                    await texto.channel.send(resposta)
            elif frase.startswith('eu sou seu pai'):
                resposta = 'Nãooooooooooooooooooooooooooooooooooooooooooo'
                await texto.channel.send(resposta)
            elif frase.startswith('codigo'):
                if texto.author.id == "367479909595086860":
                    resposta = 'Olá Sr Hudson seu codigo é:\n367479909595086860'
                    await texto.channel.send(resposta)
                else:
                    resposta = 'Acesso Negado'
                    await texto.channel.send(resposta)
            elif frase.startswith('vithoria'):
                resposta = "Essa adoraaaa pular muro"
                await texto.channel.send(resposta)
            elif frase.startswith('lucas'):
                reposta = "Esse é vacilão não cumpre o combinado"
                await texto.channel.send(resposta)
            else:
                resposta ='😩 Desculpe mas nao consigo lhe entender 😟 !!!!\n Vou tentar no Google'
                await texto.channel.send(resposta)
                respPesquisa=Google.pesquisaPorVoz(frase)
                if (respPesquisa):
                    resposta ="A pesquisa foi feita com sucesso 😎"
                    await texto.channel.send(resposta)
                else:
                    resposta ="Erro na pesquisa🤕"
                    await texto.channel.send(resposta)
