import Causador
import PossiveisRespostas
import Secreto
import discord


cliente_discord = discord.Client()
Token = Secreto.token()
Msg_id = None # None significa que a variavel esta vazia
Msg_user = None


@cliente_discord.event
async def on_ready():
    print('Bot ONLINE')

Causador.causador(cliente_discord)
PossiveisRespostas.respostas(cliente_discord)
cliente_discord.run(Token)
